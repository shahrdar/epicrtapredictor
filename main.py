import sqlite3
import numpy as np
import pandas as pd
import lightgbm as lgbm
import sklearn
import matplotlib.pyplot as plt
import seaborn as sns

conn = sqlite3.connect('inputs/rta_snapshot.db')
columns = []
for res in conn.execute('pragma table_info(battle_logs)'):
    columns.append(res[1])

ar = []
for res in conn.execute('select * from battle_logs where 2000 < SCORE'):# limit 10'):
    ar.append(res)

units = set()
for col in columns:
    if 'PICK' in col or 'BAN' in col:
        for res in conn.execute('select distinct {} from battle_logs'.format(col)):
            units.add(res[0])

servers = set()
for col in columns:
    if 'SERVER' in col:
        for res in conn.execute('select distinct {} from battle_logs'.format(col)):
            servers.add(res[0])

leagues = set()
for col in columns:
    if 'LEAGUE' in col:
        for res in conn.execute('select distinct {} from battle_logs'.format(col)):
            leagues.add(res[0])

df = pd.DataFrame(ar, columns=columns)

class DictVectorizer:
    def fit(self, X):
        self.dict2num_ = {}
        self.index2key_ = []
        X = set(X)
        for cnt, elem in enumerate(X):
            self.dict2num_[elem] = cnt
            self.index2key_.append(elem)

    def transform(self, X):
        ret = [0]*len(self.dict2num_)
        for elem in X:
            ret[self.dict2num_[elem]] = 1
        return np.array(ret)

unit_dv = DictVectorizer()
unit_dv.fit(units)
server_dv = DictVectorizer()
server_dv.fit(servers)
league_dv = DictVectorizer()
league_dv.fit(leagues)

for col in df.columns:
    if 'LOSSES' in col or 'WINS' in col or 'WIN_LOSE' == col or 'SCORE' == col or 'WIN_LOSE' == col:
        df[col] = df[col].astype(int)

pick_keys = [set(), set()]
for i in range(2):
    for col in df.columns:
        if 'P%d_PICK'%(i+1) in col:
            pick_keys[i].add(col)

ban_keys = ['P1_POSTBAN', 'P2_POSTBAN']

X1 = []
X2 = []
for _, row in df.iterrows():
    picks = [dict(), dict()]
    for i, pick_keyset in enumerate(pick_keys):
        for key in pick_keyset:
            picks[i][row[key]] = 1
        picks[i].pop(row[ban_keys[i]])

    # somehow leaking user info?
    #shared_info = [row[['SCORE']].values]
    shared_info = [0]

    p1 = unit_dv.transform(picks[0])
    p1_server = server_dv.transform([row['P1_SERVER']])
    p1_league = league_dv.transform([row['P1_LEAGUE']])
    p1_info = [p1_server, p1_league, row[['P1_WINS', 'P1_LOSSES']].values, [(row['P1_WINS']+5.0)/(row['P1_LOSSES']+10.0)]]

    p2 = unit_dv.transform(picks[1])
    p2_server = server_dv.transform([row['P2_SERVER']])
    p2_league = league_dv.transform([row['P2_LEAGUE']])
    p2_info = [p2_server, p2_league, row[['P2_WINS', 'P2_LOSSES']].values, [(row['P2_WINS']+5.0)/(row['P2_LOSSES']+10.0)]]

    tmp1 = np.hstack(shared_info + list(p1-p2) + p1_info + p2_info)
    tmp2 = np.hstack(shared_info + list(p2-p1) + p2_info + p1_info)
    X1.append(tmp1)
    X2.append(tmp2)

X1 = np.array(X1, dtype=float)
X2 = np.array(X2, dtype=float)

print(X1.shape)

names = server_dv.index2key_ + league_dv.index2key_ + ['WINS', 'LOSSES', 'WINRATE']
names = ['SCORE'] + unit_dv.index2key_ + names + names

assert(len(names) == X1.shape[1])

y1 = ((df['WIN_LOSE'].values+1)/2).astype(float)
y2 = ((-df['WIN_LOSE'].values+1)/2).astype(float)

results = np.zeros(X1.shape[1], dtype=float)

kf = sklearn.model_selection.KFold(5, shuffle=True, random_state=0x5EED)
for j, (train_index, test_index) in enumerate(kf.split(X1, y1)):
    X_train = np.vstack([X1[train_index,:], X2[train_index,:]]).astype(float)
    y_train = np.hstack([y1[train_index], y2[train_index]])

    X_test = np.vstack([X1[test_index,:], X2[test_index,:]])
    y_test = np.hstack([y1[test_index], y2[test_index]])

    kwargs = {
        #'max_depth': 5,
        #'num_leaves': 2**8-1,
        'subsample_for_bin': 2000,
        #'subsample': 0.8,
        'subsample_freq': 1,
        'random_state': 0x5EED+j,
        'importance_type': 'gain',
    }
    model = lgbm.LGBMRegressor(n_estimators=1000, learning_rate=0.01, objective='binary', **kwargs)
    model.fit(X_train, y_train)#, eval_set=(X_test, y_test), early_stopping_rounds=100)


    pred = model.predict(X_test)
    print(sklearn.metrics.roc_auc_score(y_test, pred))

    results += model.feature_importances_
results /= 5

replace = {
    'c1090': 'Ray',
    'c1101': 'Choux',
    'c1104': 'Mortelix',
    'c1105': 'Elphelt',
    'c1107': 'Kizuna Ai',
    'c3026': 'Free Spirit Tieria',
    'c4065': 'All Rounder Wanda',
    'c4071': 'Zealot Carmainerose',
    'c4073': 'Doll Maker Pearlhorizon',
    'c5004': 'Archdemons Shadow',
    'm****': 'Fodeer',
}
for i, name in enumerate(names):
    if name in replace:
        names[i] = replace[name]

feature_imp = pd.DataFrame(sorted(zip(results, names), reverse=True)[:110], columns=['value','feature'])

print(feature_imp)
plt.figure(figsize=(10, 15))
sns.barplot(x='value', y='feature', data=feature_imp)
plt.xscale('log')
plt.title('LightGBM feature importances')
plt.tight_layout()
plt.plot()
plt.savefig('fig.png')
#plt.show()
